using Test
using FatDatasets

# Only manually, as it downloads a lot of data
# include(joinpath(@__DIR__, "runtests_manual.jl"))

d = 8
n = 933

@testset "Sizes GMM datasets" begin
	X = GMM_dataset(5, d, n)
	@test size(X) == (d,n)
	S = GMM_stream(5, d, n)
	@test size(S) == (d,n)
end

@testset "Sizes lowrank datasets" begin
	X = lowrank_dataset(5, d, n)
	@test size(X) == (d, n)
	S = lowrank_stream(5, d, n)
	@test size(S) == (d,n)
end
