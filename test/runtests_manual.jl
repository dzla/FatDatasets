using Test
using FatDatasets

ENV["DATADEPS_ALWAYS_ACCEPT"] = true 

# These tests are mainly to ensure that the preprocessing runs without error.
@testset "KddCup" begin
	X = load_dataset("kddcup99_small")
	@test size(X) == (141,494021)
	X = load_dataset("kddcup99")
	@test size(X) == (145,4898431)
end
@testset "FMA" begin
	X = load_dataset("fma_mfcc")
	@test size(X) == (20,106574)
	X = load_dataset("fma")
	@test size(X) == (518,106574)
end
@testset "Gowalla" begin
	X = load_dataset("gowalla")
	@test size(X) == (2,6442892)
end
@testset "Fasttext" begin
	X = load_dataset("fasttext_fr")
	@test size(X) == (300,2000000)
end
@testset "LFW" begin
	X = load_dataset("lfw")
	@test size(X) == (62500,13233)
end
@testset "Celeba" begin
	X = load_dataset("celeba")
	@test size(X) == (38804,202599)
end
@testset "intel_lab" begin
	X = load_dataset("intel_lab")
	@test size(X) == (5,2313153)
end
