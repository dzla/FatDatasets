module FatDatasets

using Dates
using DocStringExtensions
using ImageMagick
using StatsBase
using Statistics
using Distributions
using DataFrames
using DataDeps
using CSV
using JLD
using HDF5
using CodecZlib
using ProgressMeter
using FileIO
using Images
using BatchIterators
using Random
using Distances
using LinearAlgebra
using SpecialFunctions
using PooledArrays # For detecting categorical features
using Requires

export load_dataset, list_datasets, print_datasets, folder_name
export h5file
export convert_categorical_columns!

include("utils.jl")
include("synthetic.jl")

include("datasets/celeba.jl")
include("datasets/fasttext.jl")
include("datasets/fma.jl")
include("datasets/gowalla.jl")
include("datasets/kddcup99.jl")
include("datasets/lfw.jl")
include("datasets/intel_lab.jl")

const DS_DIR = joinpath(@__DIR__, "datasets", "registrators")

function __init__()
	empty!(DS_NAMES)
	empty!(PARENT_DEPS)
	for f in walkfiles(DS_DIR)
		include(f)
	end
	# Dynamically load python stuff to avoid explicit dependency to PyCall
	@require PyCall="438e738f-606a-5dbb-bf0a-cddfbfd45ab0" begin
		using .PyCall
		include("utils_python.jl")
		export dump_npy, dump_matrix_npy
	end
end

end # module
