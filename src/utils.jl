# List of available datasets
DS_NAMES = String[]
HDF5_NAMES = String[]
# Parent datadeps for dataset name that do not correpond exactly to the datadep
PARENT_DEPS = Dict{String,String}()	

function add_dataset(n; hdf5 = false) 
	push!(DS_NAMES, n)
	if hdf5
		push!(HDF5_NAMES, n)
	end
end

list_datasets() = DS_NAMES

function print_datasets()
	for e in DS_NAMES
		println(e)
	end
end

"""
Returns the name of the datadep folder required to provide `ds`.
"""
folder_name(ds) = (ds in keys(PARENT_DEPS)) ?  PARENT_DEPS[ds] : ds

#######################################################################
#                          Loading datasets                           #
#######################################################################

center!(X) = (X .= X .- mean(X, dims=2))
"""
$(SIGNATURES)
Will load the dataset `dataset_name` and, in this order and depending on keyword arguments, center it (substract mean), standardize it (divide each attribute by its std) and then rescale the samples (so that maximum ‖xᵢ‖₂ is 1.0).

Keyword arguments include:
- center = false
- standardize = false
- rescale_columns = false
For HDF5-based datasets, one can also specify:
- ncols = Inf: a number of columns to use for the dataset. 
Note that dataset statistics need to be recomputed each time when using `ncols` together with `center`, `standardize` ou `rescale_columns`.
"""
function load_dataset(name; kwargs...) 

	X = if name in HDF5_NAMES
		load_h5(name; kwargs...)
	else
		X = load_jld(name)
		postprocess!(X; kwargs...)
	end
	return X
end
function postprocess!(X::Matrix;
					  center = false,
					  standardize = false, 
					  rescale_columns = false) 
	if center
		center!(X)
	end
	if standardize
		@assert center
		X .= X ./ std(X, dims=2)
	end
	if rescale_columns
		max_colnorm = sqrt.(maximum(sum(X.^2, dims=1)))
		X = X ./ max_colnorm
	end
	return X
end
function postprocess!(X::HDF5.Dataset; kwargs...) end

function load_jld(name)
	ddname = folder_name(name)
	dir = @datadep_str ddname
	path = joinpath(dir, "$name.jld")
	return JLD.load(path, "X")
end

#######################################################################
#                                HDF5                                 #
#######################################################################

function open_h5_file(name::String, flag="r";)
	ddname = folder_name(name)
	dir = @datadep_str ddname
	h5path = joinpath(dir, "$name.h5")
	h5f = h5open(h5path, flag)
end

"""
$(SIGNATURES)
Loads the datasaset `name` and, according to the options, center it (`center`), reduce each feature (`standardize`), and rescale the columns so that the maximum l2 column norm is 1.0 (`rescale_columns`).
"""
function load_h5(name::String, flag="r";
				 center = false,
				 standardize = false, 
				 rescale_columns = false,
				 ncols = Inf) 

	@assert ~standardize || center "Argument `standardize` can only be used with `center=true`."
	# ddname = folder_name(name)
	# dir = @datadep_str ddname
	# h5path = joinpath(dir, "$name.h5")
	# h5f = h5open(h5path, flag)
	h5f = open_h5_file(name, flag)
	d, n = size(h5f["X"])
	ds = H5Dataset(h5f["X"], nothing, nothing, nothing, false, false, false, min(n, ncols))
	if ncols < Inf
		center && compute_μ!(ds)
		ds.center = center
		standardize && compute_σ!(ds)
		ds.standardize = standardize
		rescale_columns && compute_max_colnorm!(ds)
		ds.rescale_columns = rescale_columns
	else
		ds.μ = read(h5f["mean"])
		ds.σ = read(h5f["std"])
		ds.max_colnorm = (center ? (standardize ? read(h5f["max_colnorm_centered_reduced"]) : read(h5f["max_colnorm_centered"])) : read(h5f["max_colnorm"]))
		ds.center = center
		ds.standardize = standardize
		ds.rescale_columns = rescale_columns
	end
	return ds
end

"""
	Wrapper for HDF5-stored 2d datasets. Should not be muted.
"""
mutable struct H5Dataset
	rawX::HDF5.Dataset
	μ::Union{Array{Float64}, Nothing}
	σ::Union{Array{Float64}, Nothing}
	max_colnorm::Union{Float64, Nothing}
	center::Bool
	standardize::Bool
	rescale_columns::Bool
	ncols::Int
end
Base.size(d::H5Dataset) = (size(d.rawX,1), d.ncols)
Base.size(d::H5Dataset, dim) = (dim == 1 ? size(d.rawX,1) : ((dim == 2) ? d.ncols : error("H5Dataset has only two dimensions.")))
Base.getindex(::H5Dataset, args...) = error("H5Dataset supports only 2d indexing.")
Base.eltype(X::H5Dataset) = eltype(X[1,1])
Base.length(d::H5Dataset) = size(d,1)*size(d,2)
function Base.getindex(d::H5Dataset, i1, i2)
	i2m = (i2 isa Colon) ? (1:size(d,2)) : i2
	batch = d.rawX[i1, i2m]
	d.center && (batch .-= d.μ[i1])
	d.standardize && (batch ./= d.σ[i1])
	d.rescale_columns && (batch /= d.max_colnorm)
	return batch
end
function Base.getindex(d::H5Dataset, i1, i2::Vector)
	batch = hcat([d.rawX[i1, i] for i in i2]...)
	d.center && (batch .-= d.μ[i1])
	d.standardize && (batch ./= d.σ[i1])
	d.rescale_columns && (batch /= d.max_colnorm)
	return batch
end
function StatsBase.cov(d::H5Dataset; dims = 2, corrected = true)
	@assert dims == 2 
	if d.center
		sum(b*b' for b in BatchIterator(d))/(size(d,2) - corrected)
	else
		sum((c = b .- d.μ; c*c') for b in BatchIterator(d))/(size(d,2) - corrected)
	end
end
function cov_Float32(d::H5Dataset; dims = 2, corrected = true)
	@assert dims == 2 
	if d.center
		sum((c = convert(Array{Float32}, b); c*c') for b in BatchIterator(d))/(size(d,2) - corrected)
	else
		sum((c = convert(Array{Float32}, b.-d.μ); c*c') for b in BatchIterator(d))/(size(d,2) - corrected)
	end
end

#######################################################################

"""
$(SIGNATURES)
Loads a HDF5 dataset with data at /X.
Returns a `DatasetMatrix` with potentially populated fields μ, σ, max_colnorm if keys `mean`,`std` and `max_colnorm[_centered[_reduced]]` exist in the file.
"""
function load_h5_dataset(name::String;
				 		center = false,
				 		standardize = false, 
				 		rescale_columns = false,
				 		ncols = Inf) 

	@assert ~standardize || center "Argument `standardize` can only be used with `center=true`."
	h5f = open_h5_file(name, "r")
	tryread(k) = (k in names(h5f)) ? read(h5f[k]) : nothing
	d, n_data = size(h5f["X"])
	n = min(n_data, ncols)
	max_colnorm = (center ? (standardize ? tryread("max_colnorm_centered_reduced") : 
							 				tryread("max_colnorm_centered")) : 
							tryread("max_colnorm"))
	DatasetMatrix(h5f["X"]; 
				  ncols = n,
				  center = center,
				  standardize = standardize,
				  rescale_columns = rescale_columns,
				  μ = tryread("mean"),
				  σ = tryread("std"),
				  max_colnorm = max_colnorm)
end

"""
	DatasetMatrix{T}
Wrapper type to access a (potentially out of core) dataset, that can be trimed, centered and rescaled on the fly.
"""
mutable struct DatasetMatrix{S,T} <: AbstractMatrix{T}
	data::S
	μ::Union{Array{T}, Nothing}
	σ::Union{Array{T}, Nothing}
	max_colnorm::Union{T, Nothing}
	center::Bool
	standardize::Bool
	rescale_columns::Bool
	ncols::Int
	function DatasetMatrix(X::S;
							ncols = Inf,
							center = false, 
							standardize = false,
							rescale_columns = false,
							μ = nothing,
							σ = nothing,
							max_colnorm = nothing) where S
		T = eltype(X)
		if (ncols < size(X,2)) || (μ == nothing || σ == nothing || max_colnorm == nothing)
			μ, σ, max_colnorm = compute_stats(X, ncols, center, standardize)
		end
		# println("In DatasetMatrix constructor. S=$S and T=$T")
		# println(typeof(μ))
		# println(typeof(σ))
		new{S,T}(X, μ, σ, max_colnorm, center, standardize, rescale_columns, ncols)
	end
end

"""
$(SIGNATURES)
Computes some statistics (mean, std, max norm of columns) of the dataset made of the `ncols` first columns of `X`.
"""
function compute_stats(X, ncols, center, standardize)
	d, n_raw = size(X)
	n = min(n_raw, ncols)
	# Mean
	it = BatchIterator(X; limit=n)
	μ = vec(sum(sum(b, dims=2) for b in it)./n)
	# Standard deviation
	centered_it = (b .- μ  for b in it)
	σ = vec(sqrt.(sum(sum(b.^2, dims=2) for b in centered_it)./n))
	# Columns norms
	centered_reduced_it = (b./σ  for b in centered_it)
	it_mcn = (center ? (standardize ? centered_reduced_it : centered_it) : it)
	max_colnorm = sqrt.(maximum(maximum(sum(b.^2, dims=1)) for b in it_mcn))
	return μ, σ, max_colnorm
end

Base.size(d::DatasetMatrix, args...) = error("FIX and use ncols")
Base.getindex(::DatasetMatrix, args...) = error("DatasetMatrix supports only 2d indexing.")
Base.eltype(d::DatasetMatrix) = eltype(d.data)
function Base.getindex(d::DatasetMatrix, i1, i2)
	batch = d.data[i1, i2]
	d.center && (batch = batch .- d.μ[i1])
	d.standardize && (batch = batch ./ d.σ[i1])
	d.rescale_columns && (batch = batch ./ d.max_colnorm)
	return batch
end
function StatsBase.cov(d::DatasetMatrix; dims = 2, corrected = true)
	@assert dims == 2 
	if d.center 	# Batches are already centered
		sum(b*b' for b in BatchIterator(d))/(size(d,2) - corrected)
	else	
		sum((c = b .- d.μ; c*c') for b in BatchIterator(d))/(size(d,2) - corrected)
	end
end


#######################################################################
#                            Preprocessing                            #
#######################################################################

function df_to_jld(df, jldpath)
	@debug "Saving a $(size(df)) dataframe to $jldpath."
	X = copy(Matrix(df)')
	JLD.save(jldpath, "X", X)
end

"""
$(SIGNATURES)
Removes columns of `df` which are stored as `PooledArray` (assumed to encode categorical features), and replace them with as many new binary columns as needed to encode all the possible categorical features. New columns are added at the end of the `df`.
"""
function convert_categorical_columns!(df)
	n = size(df,1)
	# In reverse order, to remove colums without any problem
	for i = size(df,2):-1:1
		if typeof(df[:,i]) <: PooledArray
			u = unique(df[:, i])
			@debug "Converting column $i ($tp) to $(length(u)) new numerical columns."
			for (j, e) in enumerate(u)
				newlabel = Symbol("Column$(i)_$j")
				df[!,newlabel] = convert(Array{Float64}, df[:, i] .== e)
			end
			select!(df, Not(i)) # Delete the original categorical column
		end
	end
	@debug "Final size is $(size(df))."
end

"""
	compute_stats(h5file, h5X)
Adds statistics `mean`, `std`, `max_colnorm` (maximum l2 norm of columns), `max_colnorm_centered`, `max_colnorm_centered_reduced` to the HDF5 file `h5file`.
"""
function compute_stats!(h5file::HDF5.File, h5X::HDF5.Dataset)
	d, n = Base.size(h5X)
	h5μ = HDF5.create_dataset(h5file, "mean", datatype(Float64), dataspace(d,1))
	h5σ = HDF5.create_dataset(h5file, "std", datatype(Float64), dataspace(d,1))
	μ = vec(sum(sum(b, dims=2) for b in BatchIterator(h5X))./n)
	centered_it = (b .- μ  for b in BatchIterator(h5X))
	h5μ[:,1] = μ
	σ = vec(sqrt.(sum(sum(b.^2, dims=2) for b in centered_it)./n))
	h5σ[:,1] = σ
	centered_reduced_it = (b./σ  for b in centered_it)
	h5file["max_colnorm"] = sqrt.(maximum(maximum(sum(b.^2, dims=1)) for b in BatchIterator(h5X)))
	h5file["max_colnorm_centered"] = sqrt.(maximum(maximum(sum(b.^2, dims=1)) for b in centered_it))
	h5file["max_colnorm_centered_reduced"] = sqrt.(maximum(maximum(sum(b.^2, dims=1)) for b in centered_reduced_it))
end

function compute_μ!(ds::H5Dataset)
	ds.μ = sum(sum(b, dims=2) for b in BatchIterator(ds))./size(ds,2)
end
function compute_σ!(ds::H5Dataset)
	@assert ds.center
	ds.σ = sqrt.(sum(sum(b.^2, dims=2) for b in BatchIterator(ds))./size(ds,2))
end
"""
Computes max_colnorm, taking into account `center` and `standardize` parameters.
"""
function compute_max_colnorm!(ds::H5Dataset)
	ds.max_colnorm = sqrt(maximum(maximum(sum(b.^2, dims=1)) for b in BatchIterator(ds)))
end

#######################################################################
#                            Misc helpers                             #
#######################################################################

walkfiles(dir) = Iterators.flatten( ((joinpath(root,file) for file in files) for (root,dirs,files) in walkdir(dir)) )

remove_extension(path) = join(split(path, ".")[1:end-1], ".")

function print_stats(df)
	for i in names(df)
		println("col. $i, min: $(minimum(df[:,i])), max: $(maximum(df[:,i]))")
	end
end

