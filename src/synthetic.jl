export RandomStreamDataset
export lowrank_stream, lowrank_dataset
export random_GMM, GMM_dataset, GMM_stream
export opt_kvar_from_intervar, opt_kvar_from_sep

"""
$(SIGNATURES)
Returns an estimate of the expected minimum squared separation between clusters.
"""
opt_kvar_from_intervar(σ²_inter, d, k) = (4*σ²_inter*gamma(1+d/2)^(2/d)*gamma(1+1/d)^2)*k^(-4/d)
"""
$(SIGNATURES)
Returns an estimate of the expected minimum squared separation between clusters.
"""
opt_kvar_from_sep(separation, d, k) = (4*(separation*k^(1/d))^2*gamma(1+d/2)^(2/d)*gamma(1+1/d)^2)*k^(-4/d)

#######################################################################
#                               Streams                               #
#######################################################################

abstract type StreamDataset end
struct RandomStreamDataset{T} <: StreamDataset
	d::Int
	n::Int
	f::Function
	rng::Random.MersenneTwister
	seed::Array{UInt32,1}
end
"""
$(SIGNATURES)

A dataset generated randomly and on the fly, but with a fixed seed so that it can be iterated over multiple times. It is possible to iterate over a RandomStreamDataset using a `BatchIterator` from BatchIterators package.
Parameter `f` should be a function which for parameters (i::Int, rng::MersenneTwister) returns a `d`×`i` matrix of elements of type `T`. The random number generator should be used sequentially, so that f(2,rng) should be the same as concatening the results of two calls to f(1,rng). 
"""
RandomStreamDataset(T::DataType, d::Int, n::Int, f::Function, rng=MersenneTwister()) = RandomStreamDataset{T}(d, n, f, rng, rng.seed)
function RandomStreamDataset(f::Function, n, rng=MersenneTwister())
	r = f()
	d = length(r)
	T = eltype(r)
	return RandomStreamDataset{T}(d, n, f, rng, rng.seed)
end
Base.size(s::RandomStreamDataset, dim = :) = (s.d, s.n)[dim]
Base.length(s::RandomStreamDataset) = prod(size(s))
Base.show(io::IO, s::RandomStreamDataset{T}) where {T} = print(io, "$(s.d) × $(s.n) RandomStreamDataset{$(T)} ($(s.n) samples in dimension $(s.d)).")

function Base.iterate(it::BatchIterator{RandomStreamDataset{T}} where T, st = 0)
	s = it.X
	println(typeof(s))
	if st == 0
		Random.seed!(s.rng, s.seed) # restart same random sequence at beginning
	end
	st = st + 1				# new state
	d = st - it.length		# > 0 means overflow, == 0 means last batch
	cbsz = (d == 0) ? mod(it.limit - 1, it.bsz) + 1 : it.bsz		# Size of current batch
	fst = (st-1)*it.bsz+1
	lst = (st-1)*it.bsz+cbsz
	if d>0
		nothing 
	else
		batch = it.X.f(cbsz, s.rng) 
		(batch, st)
	end
end

#######################################################################
#							Mixture of Gaussians 					  #
#######################################################################

"""
$(SIGNATURES)

Returns a random mixture of `k` Gaussian in dimension `d`.
Informations on the generation process can be obtained by passing a dictionary to the keyword argument `out_dic`.

Two of the following arguments can be simultaneously provided (the third being automatically computed):
- `separation`: Gaussian centers are drawn according to a Normal distribution of standard deviation `separation × k^(1/d)`.
- `intra_var`: variance of each component of the mixture (when `intra_var_balance` is set to 1.0).
- `inter_intra_var_ratio`: ratio between inter-cluster and intra-cluster variances.
By default, a separation of 2.5 and an intra-variance of 1.0 will be used.

Other parameters include:
- `Dirichlet_parameter`: controls the balance of the weights of the different clusters.
- `intra_var_balance`: if greater than 1.0 (default value), random variances will be drawn between `intra_var` / `intra_var_balance` and `intra_var` × `intra_var_balance`.

See also: [`GMMDataset`](@ref).
"""
function random_GMM(k::Int64, d::Int64; 	
				    separation = nothing, 
					centers_var = nothing,
				    inter_intra_var_ratio = nothing,
			 		Dirichlet_parameter = nothing,
			 		intra_var = nothing,
					intra_var_balance = 1.0,
					out_dic::Union{Dict, Nothing} = nothing, )

	default_separation = 2.5
	default_intra_var = 1.0
	α = if Dirichlet_parameter ≠ nothing
		rand(Dirichlet(k, Dirichlet_parameter))
	else
		1/k*ones(k)
	end
	@assert (separation == nothing || centers_var == nothing) "separation and centers_var cannot be both provided at the same time."
	if centers_var ≠ nothing
		separation = sqrt(centers_var) / (k^(1/d))
	end
	@assert any((x->isa(x,Nothing)).([separation, inter_intra_var_ratio, intra_var])) "Only two of the keywords separation, inter_intra_var_ratio and var can be specified simultaneously."
	if (separation == nothing) && (inter_intra_var_ratio == nothing || intra_var == nothing) 
		separation = default_separation
	end
	if (intra_var == nothing) && (inter_intra_var_ratio == nothing)
		intra_var = default_intra_var
	end
	@assert(sum((x->isa(x,Nothing)).([separation, inter_intra_var_ratio, intra_var])) == 1)
	if intra_var == nothing
		σ²_μ = (separation * k^(1/d))^2
		intra_var = σ²_μ / inter_intra_var_ratio
	elseif inter_intra_var_ratio == nothing
		σ²_μ = (separation * k^(1/d))^2
		inter_intra_var_ratio = σ²_μ / intra_var
	elseif separation == nothing
		σ²_μ = inter_intra_var_ratio * intra_var
		separation = sqrt(σ²_μ) / (k^(1/d))
	else @error "Error when choosing scales, should not happen." end

	# Draw Gaussian centers
	# 	Note: the covariance below refers to [Keriven et. al 2017], section 5.1 "Generating the data",
	# 	but there must be imho a square missing in "Compressive k-means" paper. 
	p_μ = MvNormal(zeros(d), σ²_μ*Diagonal(1.0I,d))
	μ = rand(p_μ, k)

	# Generate the mixture
	if intra_var_balance ≈ 1.0
		Σ = Diagonal(intra_var*I, d) 	# Same for every Gaussian
		mixture = MixtureModel(map(i -> MvNormal(μ[:,i], Σ), 1:k), α)
	else
		b = max(intra_var_balance, 1/intra_var_balance)
		f = exp.(2*rand(k).*log(b) .- log(b)) 	# between 1/b and b
		random_var = intra_var .* f
		# σ_intra = sqrt(mean(random_var))
		σ_intra = nothing
		mixture = MixtureModel(map(i -> MvNormal(μ[:,i], Diagonal(random_var[i]*I,d)), 1:k), α)
	end

	# Store informations if needed
	if out_dic ≠ nothing
		out_dic[:dataset_gmm_separation] = separation
		out_dic[:dataset_gmm_centers_var] = σ²_μ
		out_dic[:dataset_gmm_inter_var] = 2*σ²_μ
		if intra_var_balance ≈ 1.0
			out_dic[:dataset_gmm_intra_var] = intra_var
			out_dic[:dataset_gmm_intra_var_type] = :fixed
			out_dic[:dataset_gmm_inter_intra_var_ratio] = inter_intra_var_ratio
		else
			out_dic[:dataset_gmm_intra_var_type] = :random
			out_dic[:dataset_gmm_max_intra_var] = maximum(random_var)
			out_dic[:dataset_gmm_min_intra_var] = minimum(random_var)
			out_dic[:dataset_gmm_mean_intra_var] = mean(random_var)
			out_dic[:dataset_gmm_inter_mean_intra_var_ratio] = σ²_μ / mean(random_var)
		end
		# These infos are stored separately, as we don't want them in the final table.
		# TODO find a cleaner way
		non_numerical = Dict{Symbol, Any}(:truecentroids => μ)
		out_dic[:non_numerical] = haskey(out_dic, :non_numerical) ? merge(out_dic[:non_numerical], non_numerical) : non_numerical
	end
	return mixture
end

function gmm_generator(k::Int, d::Int, n::Int; 
						   out_dic = nothing,
						   compute_dists = false,
						   kwargs...)

	mixture = random_GMM(k, d; out_dic = out_dic, kwargs...)
	if out_dic ≠ nothing
		out_dic[:k_dataset] = k
		out_dic[:d] = d
		out_dic[:n] = n
		if compute_dists
			μ = hcat([mixture.components[i].μ for i in 1:length(mixture.components)]...)
			sqdists_μ = pairwise(SqEuclidean(), μ, dims=2)
			sqdists_μ[sqdists_μ .< 0.0] .= 0.0
			mask_nz = tril(trues(size(sqdists_μ)),-1) # lower-triangular off-diagonal elements
			sqdists_μ_nz = sqdists_μ[mask_nz]
			out_dic[:mean_sqeuclidean_centroids] = mean(sqdists_μ_nz)
			out_dic[:min_sqeuclidean_centroids] = minimum(sqdists_μ_nz)
			out_dic[:max_sqeuclidean_centroids] = maximum(sqdists_μ_nz)
			out_dic[:min_sqeuclidean_centroids_over_d] = out_dic[:min_sqeuclidean_centroids]/d
			out_dic[:mean_sqeuclidean_centroids_over_d] = out_dic[:mean_sqeuclidean_centroids]/d
			dists_μ_nz = sqrt.(sqdists_μ_nz)
			out_dic[:mean_euclidean_centroids] = mean(dists_μ_nz)
			out_dic[:min_euclidean_centroids] = minimum(dists_μ_nz)
			out_dic[:max_euclidean_centroids] = maximum(dists_μ_nz)
		end
		# TODO find a cleaner way than this non_numerical field
		non_numerical = Dict{Symbol, Any}(:dsGMM => mixture)
		out_dic[:non_numerical] = haskey(out_dic, :non_numerical) ? merge(out_dic[:non_numerical], non_numerical) : non_numerical
	end
	function(n=1, rng = Random.GLOBAL_RNG)
		rand(rng, mixture, n)
	end
end

"""
$(SIGNATURES)
Returns a dataset X whose  `n` columns are drawn w.r.t. a mixture of `k` Gaussians.
Additional informations on the generation process can be obtained by passing a dictionary `out_dic`.
See also: [`random_GMM`](@ref), to which keyword arguments are passed.
"""
function GMM_dataset(k, d, n; center=true, out_dic=nothing, kwargs...)
	generator = gmm_generator(k, d, n; out_dic = out_dic, kwargs...)
	X = generator(n)
	if center
		μX = vec(mean(X, dims=2))
		X .-= μX
		if out_dic ≠ nothing
			out_dic[:non_numerical][:truecentroids] .-= μX
			center_mixture!(out_dic[:non_numerical][:dsGMM], μX)
		end
	end
	return X
end

"""
Removes `μ` from the means of the components of `m`.
"""
function center_mixture!(m::MixtureModel, μ)
	for e in m.components
		e.μ .= e.μ - μ # Update the mean of the components
	end
end

function GMM_stream(k, d, n; kwargs...)
	generator = gmm_generator(k, d, n; kwargs...)
	RandomStreamDataset(Float64, d, n, generator)
end

#######################################################################
#                              Low Rank                               #
#######################################################################


"""
$(SIGNATURES)

Returns a function with parameters `(n=1, rgn=GLOBAL_RNG)`, which returns matrices of `n` samples (columns) of `d`-dimensional vectors lying in a same random `k`-dimensional subspace.
"""
function lowrank_generator(k::Int, d::Int, ν = 0.0; 
						   out_dic = nothing)
	if out_dic ≠ nothing
		out_dic[:d] = d
		out_dic[:dataset_k] = k
		out_dic[:dataset_ν] = ν
	end
	B = randn(d, k)
	function(n=1, rng = Random.GLOBAL_RNG)
		if ν > 0
			R = randn(rng, k+d, n)
			X = B*R[1:k,:] + ν*k*R[k+1:end, :]
		else
			X = B*randn(rng, k, n)
		end
		return X
	end
end

"""
	lowrank_stream(k, d, n, ν = 0.0; out_dic = nothing)
Returns a `RandomStreamDataset` which returns `n` `d`-dimensional vectors lying in a `k`-dimensional subspace.
This stream can be iterated over using a `BatchIterator` wrapper.
"""
function lowrank_stream(k::Int, d::Int, n::Int, ν::Float64 = 0.0; 
						out_dic = nothing)
	if out_dic ≠ nothing
		out_dic[:n] = n
	end
	g = lowrank_generator(k, d, ν; out_dic = out_dic)
	RandomStreamDataset(Float64, d, n, g)
end

"""
	lowrank_dataset(k, d, n; ν = 0.0)
Returns a d × n matrix `X=A+N`, where `A` is rank-`k` and `N` is i.i.d. Gaussian with standard deviation ν.
"""
function lowrank_dataset(k, d, n, ν = 0.0; 
						 l2_normalize_cols = false, 
						 out_dic = nothing)
	if (out_dic ≠ nothing) 
		out_dic[:n] = n 
		out_dic[:dataset_l2_normalized_columns] = l2_normalize_cols 
	end
	X = lowrank_generator(k, d, ν; out_dic = out_dic)(n)
	if l2_normalize_cols
		X ./= sqrt(maximum(sum(X.^2, dims=1)))
	end
	return X
end
