"""
$(SIGNATURES)
Dumps dataset `ds_name` to a numpy `.npy` file using numpy `save` function. Keyword arguments are passed to `load_dataset`.
"""
function dump_npy(ds_name::String; path="$(ds_name).npy", kwargs...)
	X = load_dataset(ds_name; kwargs...)
	dump_matrix_npy(X[:,:], path)
end
function dump_matrix_npy(X, path)
	np = pyimport("numpy")
	np.save(path, PyReverseDims(X))
end
