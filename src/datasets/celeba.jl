depname = "celeba"
function preprocess_celeba(path)
	depname = "celeba"
	filename = "img_align_celeba.zip"
	uncompressedname = "img_align_celeba"
	outname = "$(depname).h5"
	dddir = dirname(path)
	source = joinpath(dddir, filename)
	# println("source is $source")
	unpack(source)
	files_iterator() = walkfiles(joinpath(dddir, uncompressedname))
	n = mapfoldl(x->1, +, files_iterator(); init=0)
	d = 218*178
	dest = joinpath(dddir, outname)
	h5file = h5open(dest, "cw")	# output file
	h5X = HDF5.create_dataset(h5file, "X", datatype(Float64), dataspace(d, n))
	# println("Creating HDF5 file with space for a $d × $n matrix ($(d*n*8/1024^3)Gb of uncompressed data).")
	p = Progress(n, 1, "Converting the dataset to hdf5…")
	for (i, fn) in enumerate(files_iterator())
		img = Images.load(fn)
		bw_img = vec(reverse(convert(Array{Float64}, Gray.(img)), dims=1))
		if i>n
			break
		end
		h5X[:, i] = bw_img
		next!(p)
	end
	compute_stats!(h5file, h5X)	# add μ and σ to the hdf5 file
	close(h5file)
	println("Done.")
	rm(uncompressedname, recursive=true)
end
