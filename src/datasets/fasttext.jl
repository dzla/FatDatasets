function preprocess_fasttext(shortname, path)
	depname = "fasttext_$(shortname)"
	filename = "cc.$(shortname).300.vec"
	outname = "$(depname).h5"
	dest = joinpath(dirname(path), outname)
	source = joinpath(dirname(path), filename)
	unpack(path)
	n, d = parse.(Int, split(readline(source), " "))
	h5file = h5open(dest, "cw")	# output file
	h5X = HDF5.create_dataset(h5file, "X", datatype(Float64), dataspace(d, n))
	f = CSV.Rows(source; header=false, skipto=2, delim=" ", comment="\"") # input file
	p = Progress(n, 1, "Converting the dataset to hdf5…")
	for (i, row) in enumerate(f)
		v = parse.(Float64, values(row)[2:end])
		h5X[:, i] = v 		# 300 floats
		next!(p)
	end
	compute_stats!(h5file, h5X)	# add μ and σ to the hdf5 file
	close(h5file)
	rm(source)
end
