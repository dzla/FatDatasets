function preprocess_intel_lab(path)
	depname = "intel_lab"
	filename = "data.txt"
	outname = "$(depname).h5"
	dest = joinpath(dirname(path), outname)
	rm(dest, force=true)
	source = joinpath(dirname(path), filename)
	DataDeps.unpack(path)
	# idx_tokeep = [2, 5, 6, 7, 8]
	d = 5 # keep hour, temperature, humidity, light, voltage
	n = 2313153 # end of the file is incomplete
	h5file = h5open(dest, "cw")	# output file
	h5X = HDF5.create_dataset(h5file, "X", datatype(Float64), dataspace(d, n))
	f = CSV.Rows(source; header=0, delim=" ", 
				 decimal='.', 
				 # select = idx_tokeep, 
				 types=[String, String, String, String, Float64, Float64, Float64, Float64], 
				 limit=n) # input file
	p = Progress(n, 1, "Converting the dataset to hdf5…")
	timetoseconds(t) = (hour(t)*3600+minute(t)*60.0+second(t))::Float64
	for (i, row) in enumerate(f)
		st = row[2] # time in String format
		col2 = timetoseconds(Time(st[1:min(11, length(st))]))
		v = vcat(col2, convert(Vector{Float64}, values(row)[5:8]))
		h5X[:, i] = v 		# 300 floats
		next!(p)
	end
	compute_stats!(h5file, h5X)	# add μ and σ to the hdf5 file
	close(h5file)
	rm(source)
end
