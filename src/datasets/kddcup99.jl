function preprocess_kddcup99(jldname, path)
	dest = joinpath(dirname(path), jldname)
	@assert isfile(path)
	DataDeps.unpack(path)
	unpacked_name = remove_extension(path)
	# Because of some weird bug of 7zip, the name is changed
	# when using DataDeps ≥ 0.7.4
	alt1 = "$(unpacked_name)_corrected"
	alt2 = "$(unpacked_name).corrected"
	(~isfile(unpacked_name) && isfile(alt1)) && (unpacked_name = alt1)
	(~isfile(unpacked_name) && isfile(alt2)) && (unpacked_name = alt2)
	# We need to read everything to know how to convert categorical data
	df = CSV.read(unpacked_name, DataFrame ; header = 0, pool = true)
	convert_categorical_columns!(df)
	X = copy(Matrix(df)')
	JLD.save(dest, "X", X)
	rm(unpacked_name)
end

preprocess_kddcup99_large(path) = preprocess_kddcup99("kddcup99.jld", path)
preprocess_kddcup99_small(path) = preprocess_kddcup99("kddcup99_small.jld", path)
