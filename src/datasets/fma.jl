depname = "fma"
depname_mfcc = "$(depname)_mfcc"
jldpath = "$(depname).jld"
jldpath_mfcc = "$(depname_mfcc).jld"
remotefile = "fma_metadata.zip"

function preprocess_fma(path)
	depname = "fma"
	depname_mfcc = "$(depname)_mfcc"
	jldpath = "$(depname).jld"
	jldpath_mfcc = "$(depname_mfcc).jld"
	datadepdir = dirname(path)
	unpack(path)
	fpath = joinpath("fma_metadata", "features.csv")
	df = CSV.read(fpath, DataFrame; header = 1:3, skipto = 5)
	select!(df, Not(:feature_statistics_number))
	df_to_jld(df, joinpath(datadepdir, jldpath))
	# Keep only MFCC
	cols = [String(s) for s in names(df)]
	cols_mfcc = filter(x->occursin("mfcc_mean", x), cols)
	select!(df, [Symbol(st) for st in cols_mfcc])
	df_to_jld(df, joinpath(datadepdir, jldpath_mfcc))
	rm("fma_metadata", recursive=true)
end
