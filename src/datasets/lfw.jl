depname = "lfw"
filename = "lfw-deepfunneled.tgz"

function preprocess_lfw(path)
	depname = "lfw"
	filename = "lfw-deepfunneled.tgz"
	uncompressedname = "lfw-deepfunneled"
	outname = "$(depname).h5"
	dddir = dirname(path)
	source = joinpath(dddir, filename)
	unpack(source)

	files_iterator() = walkfiles(joinpath(dddir, uncompressedname))
	n = mapfoldl(x->1, +, files_iterator(); init=0)
	d = 250*250
	dest = joinpath(dddir, outname)
	h5file = h5open(dest, "cw")	# output file
	h5X = HDF5.create_dataset(h5file, "X", datatype(Float64), dataspace(d, n))
	println("Creating HDF5 file with space for a $d × $n matrix.")

	# Read images one by one and convert to grayscale vector, store in hdf5 file
	p = Progress(n, 1, "Converting the dataset to hdf5…")
	for (i, fn) in enumerate(files_iterator())
		img = load(fn)
		bw_img = vec(reverse(convert(Array{Float64}, Gray.(img)), dims=1))
		h5X[:, i] = bw_img
		next!(p)
	end
	println("Computing statistics…")
	compute_stats!(h5file, h5X)	# add μ and σ to the hdf5 file
	close(h5file)
	println("Done.")
	rm(uncompressedname, recursive=true)
end
