depname = "gowalla"
remotefile = "loc-gowalla_totalCheckins.txt.gz"

register(DataDep(
	depname,
	""" 
	Gowalla is a location-based social networking website where users share their locations by checking-in. The friendship network is undirected and was collected using their public API, and consists of 196,591 nodes and 950,327 edges. We have collected a total of 6,442,890 check-ins of these users over the period of Feb. 2009 - Oct. 2010. 
	""",
	"https://snap.stanford.edu/data/$remotefile",
	"c1c3e19effba649b6c89aeab3c1f9459fad88cfdc2b460fc70fd54e295d83ea0",
	post_fetch_method = preprocess_gowalla,
))
add_dataset(depname)
