depname = "fma"
depname_mfcc = "$(depname)_mfcc"
remotefile = "fma_metadata.zip"

register(DataDep(
	depname,
	""" 
	FMA is a dataset for music analysis. This wrapper includes the features released by the authors, but the music files will not be downloaded.
	See "FMA: A Dataset for Music Analysis", Michaël Defferrard, Kirell Benzi, Pierre Vandergheynst and Xavier Bresson.
	""",
	"https://os.unil.cloud.switch.ch/fma/$remotefile",
	"d9527a5297a65da31c5676484d5047c3e2b8a8060ce72a46e26158be736bf265",
	post_fetch_method = preprocess_fma,
))

add_dataset(depname)
add_dataset(depname_mfcc)
PARENT_DEPS[depname_mfcc] = depname
