langs = [("eo", "esperanto"), ("en", "english"), ("fr", "french"), ("de", "german")]
for (sn, fn) in langs
	ldepname = "fasttext_$(sn)"
	register(DataDep(
		ldepname,
		""" 
		This is a dataset of pre-trained word vectors for $fn, trained on Common Crawl and Wikipedia using fastText. The model was trained using CBOW with position-weights, in dimension 300, with character n-grams of length 5, a window of size 5 and 10 negatives.
		See "Learning Word Vectors for 157 Languages", Edouard Grave, Piotr Bojanowski, Prakhar Gupta, Armand Joulin and Tomas Mikolov.
		""",
		"https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.$sn.300.vec.gz",
		post_fetch_method = (x->preprocess_fasttext(sn, x)),
	))
	add_dataset(ldepname, hdf5=true)
end
