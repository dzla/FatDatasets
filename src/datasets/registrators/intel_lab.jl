ldepname = "intel_lab"
register(DataDep(
	ldepname,
	""" 
	This is a dataset of 5 features from the Intel Lab Data dataset.
	Selected features are time, temperature, humidity, light, voltage.
	Data was collected through the hard work of: Peter Bodik, Wei Hong, Carlos Guestrin, Sam Madden, Mark Paskin, and Romain Thibaux. Mark aggregated the raw connectivity information over time and generated the beautiful network layout diagram. Sam and Wei wrote TinyDB and other software for logging data. Intel Berkeley provided hardware. The TinyOS team, in particular Joe Polastre and Rob Szewczyk, provided the software infrastructure and hardware designs that made this deployment possible. 
	""",
	"http://db.csail.mit.edu/labdata/data.txt.gz",
	"d99288c8f406ca6604d359ceaa0d8adfffa79e7095061a1e27dc4399f48c7225", 
	post_fetch_method = preprocess_intel_lab,
))
add_dataset(ldepname, hdf5=true)
