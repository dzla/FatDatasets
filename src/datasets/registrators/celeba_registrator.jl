depname = "celeba"
register(DataDep(
	depname,
	""" 
	CelebFaces Attributes Dataset (CelebA) is a large-scale face attributes dataset with more than 200K celebrity images, each with 40 attribute annotations. The images in this dataset cover large pose variations and background clutter. CelebA has large diversities, large quantities, and rich annotations.
	""",
	"https://doc-0s-84-docs.googleusercontent.com/docs/securesc/ha0ro937gcuc7l7deffksulhg5h7mbp1/92lkkh4unqm4apqvscc8j71qod4pf72q/1572422400000/13182073909007362810/*/0B7EVK8r0v71pZjFTYXZWM3FlRnM?e=download",
	"46fb89443c578308acf364d7d379fe1b9efb793042c0af734b6112e4fd3a8c74",
	post_fetch_method = preprocess_celeba,
))
add_dataset(depname, hdf5=true)
