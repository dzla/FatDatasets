#######################################################################
#                            Large dataset                            #
#######################################################################

DEPNAME = "kddcup99"
DATA = "kddcup.data.gz" # server
# DATAJLD = "kddcup99.jld" # local
# preprocess_kddcup99_large = preprocess_kddcup99(DATAJLD)
register(DataDep(
	DEPNAME,
	""" 
	This is the data set used for The Third International Knowledge Discovery and Data Mining Tools Competition, which was held in conjunction with KDD-99 The Fifth International Conference on Knowledge Discovery and Data Mining. The competition task was to build a network intrusion detector, a predictive model capable of distinguishing between "bad" connections, called intrusions or attacks, and normal connections. This database contains a standard set of data to be audited, which includes a wide variety of intrusions simulated in a military network environment. 
	""",
	"http://kdd.ics.uci.edu/databases/kddcup99/$DATA",
	"3b6c942aa0356c0ca35b7b595a26c89d343652c9db428893e7494f837b274292",
	post_fetch_method = preprocess_kddcup99_large,
))
add_dataset(DEPNAME)

#######################################################################
#                            Small version                            #
#######################################################################

DEPNAME = "kddcup99_small"
DATA = "kddcup.data_10_percent.gz"
# DATAJLD = "kddcup99_small.jld"
# preprocess_kddcup99_small = preprocess_kddcup99(DATAJLD)
register(DataDep(
	DEPNAME,
	""" 
	This is the data set (10% subset) used for The Third International Knowledge Discovery and Data Mining Tools Competition, which was held in conjunction with KDD-99 The Fifth International Conference on Knowledge Discovery and Data Mining. The competition task was to build a network intrusion detector, a predictive model capable of distinguishing between "bad" connections, called intrusions or attacks, and normal connections. This database contains a standard set of data to be audited, which includes a wide variety of intrusions simulated in a military network environment. 
	""",
	"http://kdd.ics.uci.edu/databases/kddcup99/$DATA",
	"8045aca0d84e70e622d1148d7df782496f6333bf6eb979a1b0837c42a9fd9561",
	post_fetch_method = preprocess_kddcup99_small,
))
add_dataset(DEPNAME)
