depname = "lfw"
filename = "lfw-deepfunneled.tgz"

register(DataDep(
	depname,
	""" 
	Labeled Faces in the Wild is a database of face photographs designed for studying the problem of unconstrained face recognition. The data set contains more than 13,000 images of faces collected from the web. Each face has been labeled with the name of the person pictured. 1680 of the people pictured have two or more distinct photos in the data set. The only constraint on these faces is that they were detected by the Viola-Jones face detector. More details can be found in the technical report, see http://vis-www.cs.umass.edu/lfw/.
	This version of the dataset contains the deep funneled 250*250px images.
	""",
	"http://vis-www.cs.umass.edu/lfw/$filename",
	"08575363d69edaed9a7c3ccb9e2eebd76c91bc1011b2baaf725f8d1284bf3a2f",
	post_fetch_method = preprocess_lfw,
))
add_dataset(depname, hdf5=true)
