depname = "gowalla"
jldpath = "$(depname).jld"
# uncompressedfile = "loc-gowalla_totalCheckins.txt"

function preprocess_gowalla(path)
	depname = "gowalla"
	jldpath = "$(depname).jld"
	datadepdir = dirname(path)
	unpack(path)
	source = joinpath(datadepdir, "loc-gowalla_totalCheckins.txt")
	# For 7zip
	alt1 = joinpath(datadepdir, "Gowalla_totalCheckins.txt")
	(~isfile(source) && isfile(alt1)) && (source = alt1)
	@assert isfile(source) "File $source does not exist (path=$path)."
	# Read file and convert
	df = CSV.read(source, DataFrame; header = 0)
	select!(df, [:Column3, :Column4])
	df_to_jld(df, joinpath(datadepdir, jldpath))
	rm(source)
end
